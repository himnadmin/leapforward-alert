/*******************************************************************************
MySQL class for database interaction
Alexander Self
5/29/18
Methods:
  connect(): connects to database
  end(): closes connection to database
  selectEmployees(): Selects BU employees
*******************************************************************************/
"use strict";
const con = require('./config');

class Employee {
  constructor(con) {
    this.con = con;
  }

  connect() {
    return con.connect((err) => {
      if (err) {
        console.log("Error connecting to database");
        return;
      }
      console.log('Connected to MySQL');
    });
  }// end connect

  close() {
    return con.end((err) => {
      if (err) {
        console.log("Error closing database");
        return;
      }
      console.log('Closing connection.');
    });
  }// end close

  /* Fetch all BU employees */
  selectEmployees() {
    return new Promise((resolve, reject) => {
      con.query(`SELECT phone FROM employee WHERE type="BU" AND employeeStatus="Active"`, (err, rows, fields) => {
        if (err) reject(err);
        resolve(rows);
      });
    });
  }


}// end db

module.exports = Employee;
