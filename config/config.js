/************************************************************************
ADD Twilio Credentials Here!
config.js: Credentials to interact with Twilio API
************************************************************************/

/* Phone number, secret token, Account sid */
module.exports = {
  phone: '+1XXXXXXXXXX',
  authToken: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
  accountSid: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
};

/* END */
